import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;
import java.util.Scanner;

/**
 * Classe responsável por implementar a resolução de busca 
 * em Largura e A*, tendo como base um arquivo .txt
 * 
 * @author Nairan Omura<nairanomura@hotmail.com>
 * @author Bruno Rodrigues<rodrigues_bruno96@hotmail.com>
 * @since 17/03/2019
*/
public class Principal {

	public static HashMap<String, Route> graph = new HashMap<String, Route>();
	public static String fileName = "mapa.txt";
	public static String origem;
	public static String destino;
	public static List<City> queue = new ArrayList<City>();
	public static List<LinkedList<Route>> walked = new ArrayList<LinkedList<Route>>();

	public static void main(String[] args) {
		graph = getRoutesFromFile(getCitysFromFile()); // Monta grafo

		Scanner input = new Scanner(System.in); // lê origem e destino
			System.out.println("Origem: ");
			origem = input.next().toUpperCase();
			System.out.println("Destino: ");
			destino = input.next().toUpperCase();
		input.close();
		
		System.out.println("\n\n***** PERCORRE GRAFO EM LARGURA *****");
		breadthFirstSearch( // realiza busca em largura
			Util.getCityOfRoute(origem), 
			Util.getCityOfRoute(destino),
			new ArrayList<City>() // guarda nós visitados
		);

		System.out.println("\n\n***** PERCORRE GRAFO EM A* *****");
		queue.add(Util.getCityOfRoute(origem));
		searchAStar( // realiza busca A*
			Util.getCityOfRoute(origem), 
			Util.getCityOfRoute(destino),
			new ArrayList<City>(), // guarda nós visitados
			0
		);
	}

	/**
	 * Realiza busca em largura SEM levar em consideração
	 * heurísticas;
	 * 
	 * @author Nairan Omura<nairanomura@hotmail.com>
	 * @author Bruno Rodrigues<rodrigues_bruno96@hotmail.com> 
	 * @since 19/03/2019
	 * 
	 * @see https://www.youtube.com/watch?v=cUlDbC0KrQo&t=102s
	 * 
	 * @param graph Grafo com todos os nós
	 * @param start Nó inicial
	 * @param end Nó final
	 */
	private static void breadthFirstSearch(City start, City end, ArrayList<City> visited) {
		Queue<City> queue = new LinkedList<City>();
		
		/** custo computacional, começa em 1 pois já considera o nó inicial, não considera já visitados */
		int computationalValue = 1; 
		
		// Inserir custo real, soma dos caminhos percorridos até o destino
		visited.add(start);
		queue.add(start);

		List<LinkedList<Route>> percurso = new ArrayList<LinkedList<Route>>();

		while (!queue.isEmpty()) {
			City element = queue.remove();
			Route actually = graph.get(element.getId());

			/** Pega todos os nós adjacentes */
			List<Route> neighbours = graph.get(element.getId()).getDestiny();
			/** Remove objetos nulos da lista */
			neighbours.removeIf(Objects::isNull); 
			
			//System.out.println("Nó: " + element.getId());
		
				for (Route node : neighbours) {
					City adjNode = node.getOrigin();

					/** Marca percurso percorrido */
					LinkedList<Route> linkedList = new LinkedList<Route>();
					linkedList.addFirst(actually);
					linkedList.addLast(node);
					percurso.add(linkedList);

					//System.out.println("Filho:" + actually.getOrigin().getId() + " => " + node.getOrigin().getId());

					if (node.getOrigin() == end) {

						//System.out.println("    ENCONTRADO: " +node.getOrigin().getId());
						computationalValue += 1;
						/** ROTA PERCORRIDA */
						List<String> route = Util.getCourseOfElements(percurso, start, end, new ArrayList<String>());
						double realCoast = 0;
						System.out.print("ROTA: ");
						Route testaCusto = graph.get(route.get(route.size()-1));
						System.out.print(route.get(route.size()-1) + " => ");

						for (int index = route.size() -2; index>= 0 ; index--) {
							System.out.print(route.get(index) + " => ");

							List<Route> listaDestinos = testaCusto.getDestiny();
							for(Route custo : listaDestinos) {
								if (custo.getOrigin().getId() == route.get(index)) {
									realCoast += custo.getDistance();
									testaCusto = graph.get(route.get(index));
									break;
								}
							}
						}
						System.out.println("\nCusto Computacional: " + computationalValue);
						System.out.println("Custo Real: " + realCoast);
						return;
					}

					if (!visited.contains(adjNode)) {
						/** Impressão dos nós */
						//System.out.println("    vizinho: " + adjNode.getId());

						/** Marca nó como visitado e adiciona a fila */
						visited.add(adjNode);
						queue.add(adjNode);

						computationalValue += 1;
					} else {
						//System.out.println("    (!já visitado): " + adjNode.getId());
					}
				}
		}
	}

	/**
	 * Realiza busca A* levando em consideração as
	 * heurísticas;
	 * 
	 * Inicialize Q com o nó de busca (S) como única entrada;
	 * Se Q está vazio, interrompa. Se não, escolha o melhor elemento de Q;
	 * Se o estado (n) é um objetivo, retorne n;
	 * (De outro modo) Remova n de Q;
	 * Encontre os descendentes do estado (n) que não estão em visitados e crie todas as extensões de n para cada descendente;
	 * Adicione os caminhos estendidos a Q e vá ao passo 2;
	 * @author Nairan Omura<nairanomura@hotmail.com>
	 * @author Bruno Rodrigues<rodrigues_bruno96@hotmail.com> 
	 * @since 19/03/2019
	 * 
	 * @see https://www.youtube.com/watch?v=FU6JQaRMMDM
	 * 
	 * @param graph Grafo com todos os nós
	 * @param start Nó inicial
	 * @param end Nó final
	 */
	private static List<City> searchAStar(City start, City end, List<City> visited, int computationalValue) {
 
		if (queue.isEmpty() || start == end ) { // Condições de parada e resultado
			List<Route> route = Util.getRouteToOrigin(end, walked.size()-1, new ArrayList<Route>());
			int routeCoast = Util.getCoastOfRoute(route, route.size()-1, 0);
			Collections.reverse(route);

			System.out.print("ROTA: ");
			for (Route value: route) System.out.print(value.getOrigin().getId() + " => ");

			System.out.println("\nCusto Real: " + routeCoast);
			System.out.println("Custo Computacional: " + computationalValue);
			return null;
		} 

		visited.add(start); // marca os nós visitados
		queue.remove(queue.indexOf(start)); // remove nós percorridos da fila

		/** Adiciona filhos do elemento atual para fila */
		for (Route route: graph.get(start.getId()).getDestiny()) {
			if (!visited.contains(route.getOrigin())) {
				queue.add(route.getOrigin());

				LinkedList<Route> walk = new LinkedList<Route>();
				walk.addFirst(graph.get(start.getId()));
				walk.addLast(graph.get(route.getOrigin().getId()));
				if (!walked.contains(walk)) walked.add(walk);
			}
		}

		double HeuristicMinorValue = 0; 
		City minorChild = start;
		for (City adjNode : queue) { // Encontra o valor do menor filho do elemento atual
			double heuristicValue = Util.euclidianDistance(adjNode, end);

			/** nó atual => nó origem = valor até o momento */
			List<Route> route = Util.getRouteToOrigin(adjNode, walked.size()-1, new ArrayList<Route>());
			int routeCoast = Util.getCoastOfRoute(route, route.size()-1, 0);
			
			/** F(n) = G(n) + H(n)*/
			if (HeuristicMinorValue > (routeCoast + heuristicValue) || minorChild == start) {
				minorChild = adjNode;
				HeuristicMinorValue = (routeCoast + heuristicValue);
			} 
		}
		return searchAStar(minorChild, end, visited, ++computationalValue);
	}

	/**
	 * Retorna lista das cidades do arquivo base 
	 * de acordo com o parâmetro recebido
	 * 
	 * @author Nairan Omura<nairanomura@hotmail.com>
	 * @author Bruno Rodrigues<rodrigues_bruno96@hotmail.com> 
	 * @since 17/03/2019
	 * 
	 * @param FileName Nome do arquivo que será lido
	 * @return cityList Lista com as cidades encontradas
	 */
	private static HashMap<String, City> getCitysFromFile() {
		HashMap<String, City> cityList = new HashMap<String, City>();

		try {
			/** Le o arquivo */
			FileReader ler = new FileReader(fileName);
			BufferedReader reader = new BufferedReader(ler);
			String linha;

			while ((linha = reader.readLine()) != null) {
				if (linha.contains("city")) {
					String result[] = linha.substring(
						linha.indexOf('=') + 1, 
						linha.lastIndexOf(';')
					).replace("(", ",").replace(")", "").split(",");

					if (result != null) {
						cityList.put(result[0], new City(
							result[0],
							Integer.parseInt(result[1]),
							Integer.parseInt(result[2])
						));
					}
				} 
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return cityList;
	}

	/**
	 * Retorna lista das rotas do arquivo base 
	 * de acordo com o parâmetro recebido
	 * 
	 * @author Nairan Omura<nairanomura@hotmail.com>
	 * @author Bruno Rodrigues<rodrigues_bruno96@hotmail.com>
	 * @since 17/03/2019
	 * 
	 * @param FileName Nome do arquivo que será lido
	 * @return routeList Lista com as rotas e heurísticas encontradas
	 */
	private static HashMap<String, Route> getRoutesFromFile(HashMap<String, City> cityList) {
		HashMap<String, Route> routeList = new HashMap<String, Route>();

		try {
			/** Le o arquivo */
			FileReader ler = new FileReader(fileName);
			BufferedReader reader = new BufferedReader(ler);
			String linha;

			while ((linha = reader.readLine()) != null) {
				if (linha.contains("route")) {
					String result[] = linha.substring(
						linha.indexOf('=') + 1, 
						linha.lastIndexOf(';')
					).replace(";", ",").replace("-", ",").split(",");


					/** Procura rota de origem e destino */
					City origin = cityList.get(result[0]);
					City destiny = cityList.get(result[1]);

					String originID = origin.getId();
					String destinyID = destiny.getId();
	
					/** Verifica rotas de origem e destino */
					Route routeOrigin = routeList.get(originID);
					Route routeDestiny = routeList.get(destinyID);
					
					/** Adiciona origem na lista de rotas caso ainda não exista */
					if (routeOrigin == null) {
						routeList.put(originID, new Route(
							origin, 
							new ArrayList<Route>(), 
							Integer.parseInt(result[2])
						));

						routeOrigin = routeList.get(originID);
					}
					
					/**Verifica se origem já existe na lista de rotas */
					List<Route> listRoute = new ArrayList<Route>();
					if (routeDestiny != null) {
						listRoute = routeDestiny.getDestiny();
					}
					if (!listRoute.contains(routeOrigin)) {
						listRoute.add(routeOrigin);
					}

					routeList.put(destinyID, new Route(
						destiny, 
						listRoute, 
						Integer.parseInt(result[2])
					));

					/**Verifica se origem já não está setado no destino */
					Route originDestiny = routeList.get(originID);
					listRoute = originDestiny.getDestiny();
					
					if (!listRoute.contains(originDestiny)) {
						listRoute.add(routeList.get(destinyID));
						routeList.get(originID).setDestiny(listRoute);
					}
				}
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return routeList;
	}
}
