
public class City {
	
	public String id;
	public Integer valorX;
	public Integer valorY;

	public City() { }
	
	public City(String id, Integer valorX, Integer valorY) {
		this.id = id;
		this.valorX = valorX;
		this.valorY = valorY;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getValorX() {
		return valorX;
	}
	public void setValorX(Integer valorX) {
		this.valorX = valorX;
	}
	public Integer getValorY() {
		return valorY;
	}
	public void setValorY(Integer valorY) {
		this.valorY = valorY;
	}
}
