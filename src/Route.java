import java.util.List;

public class Route {

	protected List<Route> destiny;
	protected City origin;
	protected int distace;
	
	public Route() {}
	public Route(City origin, List<Route> destiny, int distace) {
		this.origin = origin;
		this.destiny = destiny;
		this.distace = distace;
	}
		
	public City getOrigin() {
		return origin;
	}
	public void setOrigin(City origin) {
		this.origin = origin;
	}
	public List<Route> getDestiny() {
		return destiny;
	}
	public void setDestiny(List<Route> destiny) {
		this.destiny = destiny;
	}
	public int getDistance() {
		return distace;
	}
	public void setDistace(int distace) {
		this.distace = distace;
	}
}
