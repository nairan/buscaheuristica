import java.util.LinkedList;
import java.util.List;

public class Util {
    
    /** 
	 * Procura e retorna uma cidade de acordo com o valor
	 * de uma string recebido por parâmetro.
	 * 
	 * @author Nairan Omura<nairanomura@hotmail.com>
   * @author Bruno Rodrigues<rodrigues_bruno96@hotmail.com> 
	 * @since 26/03/2019
	 * 
	 * @return Cidade encontrada de acordo com o parâmetro recebido
	 */
	public static City getCityOfRoute(String city) {
		if (!Principal.graph.isEmpty()) return Principal.graph.get(city).getOrigin();
		return null;
    }
    
    /**
	 * Calcula a distância euclidiana entre duas coordenadas.
	 * 
	 * @author Nairan Omura<nairan.omura@conseiller.com.br>
	 * @author Bruno Rodrigues<rodrigues_bruno96@hotmail.com>
	 * 
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return Distancia entre duas coordenadas
	 */
		public static double euclidianDistance(City origin, City destiny) {		
				return Math.sqrt(
						Math.pow((destiny.getValorX() - origin.getValorX()), 2) + 
						Math.pow((destiny.getValorY() - origin.getValorY()), 2)
				);
		}
    
    /**
     * Calcula o custo de um ponto final até sua origem.
     * 
     * @author Nairan Omura<nairanomura@hotmail.com>
 	   * @author Bruno Rodrigues<rodrigues_bruno96@hotmail.com> 
     * @since 26/03/2019
     * 
     * @param route
     * @param contador
     * @param total
     * @return Valor total da rota percorrida
     */
    public static int getCoastOfRoute(List<Route> route, int contador, int total) {
		if (route.size() == 0 || contador == 0) return total;

		Route origin = Principal.graph.get(route.get(contador).getOrigin().getId());
		Route destino = Principal.graph.get(route.get(--contador).getOrigin().getId());
		
		for (Route value : origin.getDestiny()) {
			if (value.getOrigin() == destino.getOrigin()) {
				total += value.getDistance(); 
			}
		}
		return getCoastOfRoute(route, contador, total);
    }
    
    /**
	 * Calcula o valor do nó atual até o nó de origem.
	 * 
	 * @author Nairan Omura<nairanomura@hotmail.com>
 	 * @author Bruno Rodrigues<rodrigues_bruno96@hotmail.com>
	 * @since 26/03/2019
	 * 
	 * @param node Nó de origem, que será calculado até a origem
	 * @param contador Contador auxiliará a percorrer os nós já percorridos
	 * @param total Valor total que será retornado pelo método
	 * @return Valor total do nó atual até o nó origem
	 */
	public static List<Route> getRouteToOrigin(City node, int contador, List<Route> total) {
		if (Principal.walked.size() == 0 || contador < 0) return total;

		LinkedList<Route> atual = Principal.walked.get(contador);
		Route atualOrigem = atual.getFirst();
		Route atualDestino = atual.getLast();

		if (node == atualDestino.getOrigin()) {
			if (!total.contains(atualDestino)) total.add(atualDestino);
			if (!total.contains(atualOrigem)) total.add(atualOrigem);
			node = atualOrigem.getOrigin();
		}

		if (Principal.origem.equals(node.getId())) return total;
		return getRouteToOrigin(node, --contador, total);
    }
    
    /**
	 * Busca o percurso realizado pelo algoritmo, a partir do nó final
	 * até o nó inicial.
	 * 
	 * @author Nairan Omura<nairan.omura@conseiller.com.br>
	 * @author Bruno Rodrigues<rodrigues_bruno96@hotmail.com>
	 * @since 21/03/2019
	 */
	public static List<String> getCourseOfElements(List<LinkedList<Route>> percurso, City start, City end, List<String> retorno) {
		if (percurso.isEmpty()) return null;
		LinkedList<Route> rota = percurso.remove(percurso.size() - 1);
		City origem = rota.getFirst().getOrigin();
		City destino = rota.getLast().getOrigin();
		
		if (end == destino) {
			if (!retorno.contains(destino.getId())) retorno.add(destino.getId());
			if (!retorno.contains(origem.getId())) retorno.add(origem.getId());
			end = origem;
		}
		if (start == end) return retorno;
		return getCourseOfElements(percurso, start, end, retorno);
	}
}